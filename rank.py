path = "Data.txt"
students = {}
student_data = """student marks1 marks2 marks3
A 12 14 16
B 5 6 7
C 17 20 23
F 7 8 9
G 4 5 6"""
def reading_file(student_data) :
    #return [[line[0],line[1:].split()] for line in open(path)]

    #student_data = open(path)
    for line in student_data.strip().split('\n')[1:]:
        parts = line.split()
        name = parts[0]
        marks = list(map(int, parts[1:]))
        students[name] = marks
def compare_students(student1, student2):
        mr1 = students[student1]
        mr2 = students[student2]
        return all(m1 > m2 for m1, m2 in zip(mr1, mr2))
def determine_order(students_list):
        ordered_students = []
        i = 0
        non_ordered_stds =[]
        while students_list:
            for student in students_list:
                if all(compare_students(student, other) or student == other  for other in students_list):
                    ordered_students.append(student)
                    students_list.remove(student)
                    break
                # else:
                    non_ordered_stds.append(student)
                    students_list.remove(student)
                    break
                     
        return ordered_students, non_ordered_stds
reading_file(student_data)
students_list = list(students.keys())
print(students_list)
print(determine_order(students_list))
