path = "data.txt"

def reading_file(path: str) -> list[list]:
     data =  [record.split() for record in open(path).readlines()]
     return [[name ,[int(mark) for mark in marks]] for name, *marks in data]

def check_greater(rec1: list[str, list[int]], rec2: list[str, list[int]]) -> bool:
    return  [rec1 > rec2 for rec1, rec2 in zip(record1[1], record2[1])].count(True) == len(rec1[1])

def check_smaller(rec1: list[str, list[int]], rec2: list[str, list[int]]) -> bool:
    return [rec1 < rec2 for rec1, rec2 in zip(record1[1], record2[1])].count(True) == len(rec1[1])

def separate_records():
    return 
